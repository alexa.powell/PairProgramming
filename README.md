# Pair Programming Week

A set of challenges completed using Test-Driven-Development and Pair-Programming.

Challenges are found [here](https://gitlab.com/thgaccelerator/Core_Curriculum/blob/master/technical_content/agile/pairing/pairing_challenge.md)

### Challenges

- [Fizz Buzz](/Fizzbuzz)
    - Completed by Elena Waite and James Young
    - Monday 26th November 2018

- [Tamagotchi]
