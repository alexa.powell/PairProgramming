def fizz_buzz(number):
    if number % 7 == 0 and number % 5 == 0 and number % 3 == 0:
        return 'FizzBuzzPop'
    if number % 7 == 0 and number % 3 == 0:
        return 'FizzPop'
    if number % 7 == 0 and number % 5 == 0:
        return 'BuzzPop'
    if number % 15 == 0:
        return 'FizzBuzz'
    if number % 5 == 0:
        return 'Buzz'
    if number % 3 == 0:
        return 'Fizz'
    if number % 7 == 0:
        return 'Pop'
    

    return number

